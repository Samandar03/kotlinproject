package uz.samandar

import io.vertx.core.AbstractVerticle
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicInteger

class Server : AbstractVerticle() {
    var atomicInteger = AtomicInteger(0)
    lateinit var result:String
    lateinit var cell:Cell

    override fun start() {
        vertx.createHttpServer()
            .requestHandler { req ->

                val a = req.getParam("a")
                val b = req.getParam("b")
                cell=Cell(a.toInt(),b.toInt())
                result=board.fire(cell)
                req.response()
                    .putHeader("content-type", "text/plain")
                    .end("$result")

                println("${atomicInteger.getAndIncrement()}) connected ${req.remoteAddress()}")
            }.listen(8080)
    }
}