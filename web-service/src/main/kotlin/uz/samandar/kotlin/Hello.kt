package uz.samandar

import io.vertx.core.Vertx
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClient.create

fun main(args: Array<String>) {
    println("Hello, World")
    val vertx = Vertx.vertx()
    vertx.deployVerticle(Server())

    var canWork = true
    var client = create(vertx)
    while(canWork)
    {

        val(a, b) = readLine()!!.split(" ")

        //val a = 100
        //val b = 100

        // Send a GET request
        /*client.get(8080, "localhost", "/?a=$a&b=$b").send({ ar ->
            if (ar.succeeded()) {
                // Obtain response
                var response = ar.result()

                println("Received ${response.body()}")
            }
        })

         */
        client.get(8080, "localhost", "/?a=$a&b=$b").send{ ar ->
            if (ar.succeeded()) {
                // Obtain response
                var response = ar.result()

                println("Received ${response.body()}")
            }
        }
    }
}

