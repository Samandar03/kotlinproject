package uz.samandar

var board=Board()
const val CELL_STATUS_EMPTY="Empty"
const val CELL_STATUS_FIRED="Fired"
const val CELL_STATUS_OWNED="Owned"
const val CELL_STATUS_SHIPPED="Shipped"
const val CELL_STATUS_USED="Used"

const val WARNING_OUT_OF_GAME_ZONE="Wrong Cell. Out of Game Zone"

const val GAME_OVER="Game Over!!!"


const val SHIP_DIRECTION_NORTH="N"
const val SHIP_DIRECTION_SOUTH="S"
const val SHIP_DIRECTION_WEST="W"
const val SHIP_DIRECTION_EAST="E"

const val SHIP_STATUS_DIED="Died"
const val SHIP_STATUS_MISSED="Missed"
const val SHIP_STATUS_FIRED="Fired"
const val SHIP_STATUS_ALIVE="Alive"

const val BOARD_BOUNDARY_MAX_X=10
const val BOARD_BOUNDARY_MIN_X=1
const val BOARD_BOUNDARY_MAX_Y=10
const val BOARD_BOUNDARY_MIN_Y=1

