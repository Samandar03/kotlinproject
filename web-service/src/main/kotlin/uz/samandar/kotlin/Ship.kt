package uz.samandar
class Ship(var size: Int) {
    var cells = arrayListOf<Cell>()

    var dx = 0
    var dy = 0

    fun make(cell: Cell, direction: String): Boolean {
        val temporaryCells = arrayListOf<Cell>()
        checkDirection(direction)
        makeCells(cell, temporaryCells)
        if (checkBoundary(temporaryCells)) {
            cells = temporaryCells
            return true
        }
        return false
    }

    private fun checkBoundary(temporaryCells: ArrayList<Cell>): Boolean {
        for (cell in temporaryCells) {
            if (cell.x < BOARD_BOUNDARY_MIN_X
                || cell.x > BOARD_BOUNDARY_MAX_X
                || cell.y < BOARD_BOUNDARY_MIN_Y
                || cell.y > BOARD_BOUNDARY_MAX_Y
            ) return false
        }
        return true
    }

    private fun checkDirection(direction: String) {
        when (direction) {
            SHIP_DIRECTION_NORTH -> dy = -1// WAS 1  перевернутый ось ординат
            SHIP_DIRECTION_EAST -> dx = 1
            SHIP_DIRECTION_WEST -> dx = -1
            SHIP_DIRECTION_SOUTH -> dy = 1// WAS -1
        }
    }


    private fun makeCells(cell: Cell, temporaryCells: ArrayList<Cell>) {
        var cellocal = cell
        temporaryCells.add(cell)
        cellocal.status=CELL_STATUS_OWNED
        for (k in 2..size) {
            cellocal = Cell(cellocal.x + dx, cellocal.y + dy)
            cellocal.status=CELL_STATUS_OWNED
            temporaryCells.add(cellocal)
        }
    }

    fun fire(cell: Cell): String {
        for (cellInList in cells) {
            if (cellInList.ravno(cell)) {
                cellInList.status = CELL_STATUS_FIRED
                cell.status=CELL_STATUS_FIRED
            }
        }

        if(cell.status==CELL_STATUS_FIRED){
            if(shipStatus()==SHIP_STATUS_ALIVE){
                return SHIP_STATUS_FIRED
            }
            else{
                return SHIP_STATUS_DIED
            }
        }
        else
        {
            return SHIP_STATUS_MISSED
        }


    }

    fun shipStatus():String{
        var countFired=0
        for(cellInShip in cells)
        {
            if(cellInShip.status==CELL_STATUS_FIRED)
                countFired++
        }
        if(countFired==size)
            return SHIP_STATUS_DIED
        else
            return SHIP_STATUS_ALIVE
    }
}