package uz.samandar

import io.vertx.core.Vertx
import io.vertx.ext.web.client.WebClient

fun main(args:Array<String>){
    var result:String
    var status:Boolean
    var cell:Cell
    var isWin=0
    var gameOver:Boolean=true
    println("Please enter Ship's coordinates ")
    println("Example : n->size x y (N,E,W,S)->direction")
    var builtShip:Int=1
    while(builtShip!=11)
    {
        print("#$builtShip ")
        var (size,a,b,direction)= readLine()!!.split(" ")
        cell=Cell(a.toInt(),b.toInt())
        when(direction){
            "E"->SHIP_DIRECTION_EAST
            "S"->SHIP_DIRECTION_SOUTH
            "W"->SHIP_DIRECTION_WEST
            else->SHIP_DIRECTION_NORTH
        }
        status=board.addShips(size.toInt(),cell,direction)
        if(status) {
            print(" " + status)
            builtShip++
            println()
        }
        else {
            println()
        }
    }
  /*  while(gameOver){
        var (a,b)= readLine()!!.split(" ")
        cell=Cell(a.toInt(),b.toInt())
        result=board.fire(cell)
        println(result)
        if(result==SHIP_STATUS_DIED)
        {
            isWin++
        }
        if(isWin==10){
            gameOver=false
            println("Game is Over!!! You Loose!!")
        }
    }*/
    val vertx = Vertx.vertx()
    vertx.deployVerticle(Server())
    var client = WebClient.create(vertx)
    while(gameOver){
        val(a, b) = readLine()!!.split(" ")
        client.get(8080, "192.168.43.57", "/?a=$a&b=$b").send{ ar ->
            if (ar.succeeded()) {
                // Obtain response
                var response = ar.result()

                println("Received ${response.body()}")
                if(response.body().toString()== SHIP_STATUS_DIED){
                    isWin++;
                }
                if(isWin==10){
                    gameOver=false
                    println(GAME_OVER)
                }
            }
        }
    }

}