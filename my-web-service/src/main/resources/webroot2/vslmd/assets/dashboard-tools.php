<?php
add_action( 'wp_dashboard_setup', 'vm_dashboard_add_widget_visualmodo_tools' );
function vm_dashboard_add_widget_visualmodo_tools() {
	wp_add_dashboard_widget( 
		'vm_dashboard_widget_tools', 
		__( 'Visualmodo Tools', 'vslmd' ), 
		'vm_tools'
	);
}

function vm_tools() {
	?>
	<div class="welcome-panel" style="border: none; box-shadow:none; margin: 0; padding: 0;">

	<div class="welcome-panel-content">

		<h2>Wecome to Visualmodo Tools!</h2>
		<p class="about-description">We’ve assembled some links to get you started:</p>

		<div class="welcome-panel-column" style="margin-top:16px;">
			<ul>
				<li><a href="https://icons.visualmodo.com" class="welcome-icon dashicons-star-filled" target="_blank">Free Icon Pack.</a></li>
				<li><a href="https://awards.visualmodo.com" class="welcome-icon dashicons-megaphone" target="_blank">Your Site In Our Gallery.</a></li>
			</ul>
		</div>

		<div class="welcome-panel-column" style="margin-top:16px;">
			<ul>
				<li><a href="https://shots.visualmodo.com" class="welcome-icon dashicons-camera-alt" target="_blank">Download Photos Now.</a></li>
				<li><a href="https://svg.visualmodo.com" class="welcome-icon dashicons-art" target="_blank">SVG Animation.</a></li>
			</ul>
		</div>

	</div>

	</div>
	<?php
}