<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Table</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Samandar">
    <link rel="stylesheet" href="/webroot/css/foundation.css">
    <link rel="stylesheet" href="/webroot/css/style.css">
</head>
<body>

<div id="Order of Fire">
    <#if fireTurn==player>
        <h6 style="text-align: right;color: brown;">Player#${player} is allowed to fire !!!</h6>
    </#if>
    <#if fireTurn!=player>
        <h6 style="text-align: right;color: lightcoral;">Player#${player} is NOT allowed to fire !!!</h6>
    </#if>
</div>
<br>
<br>
<div id="HeaderOpponent" >
    <h5 style="text-align: center;">OpponentBoard</h5>
</div>
<br>
<div id="OpponentTable" style="text-align: center">
    <#assign k=1>
    <#list opponentBoard as innerArray>
        <div class="tiny-2 columns">
            <#assign l=1>
            <#list innerArray as itemValue>
                <#if k==11>
                    <a href="/welcome/${player}/play/0/0/0" class="radius small alert button">X</a>
                </#if>
                <#if l==11>
                    <a href="/welcome/${player}/play/0/0/0" class="radius small alert button">X</a>
                </#if>
                <#if k!=11&&l!=11>
                    <a href="/welcome/${player}/play/${l}/${k}/0/0" class="radius small secondary button">${itemValue}</a>
                </#if>
                <#assign l=l+1>
            </#list>
        </div>
        <#assign k=k+1>
    </#list>
</div>

<div id="fireResult">
    <#if fireResult!="NULL">
        <#if fireResult=="Fired">
            <h6 style="background-color: lawngreen;text-align: center;">Fired</h6>
        </#if>
        <#if fireResult=="Missed">
            <h6 style="background-color: red;text-align: center;">Missed</h6>
        </#if>
        <#if fireResult=="Used">
            <h6 style="background-color: lightblue;text-align: center;">Used</h6>
        </#if>
        <#if fireResult=="Died">
            <h6 style="background-color: green;text-align: center;">Ship Died</h6>
        </#if>
    </#if>
</div>

<br>
<div id="HeaderCaptain">
    <h5 style="text-align: center;">CaptainBoard</h5>
</div>
<div id="CaptainTable" style="text-align: center">
<#assign i=1>
<#list captainBoard as innerArray>
    <div class="tiny-2 columns">
        <#assign j=1>
        <#list innerArray as itemValue>
            <#if i==11>
                <a href="#" class="radius small alert button">X</a>
            </#if>
            <#if j==11>
                <a href="#" class="radius small alert button">X</a>
            </#if>
            <#if i!=11&&j!=11>
                <a href="#" class="radius small secondary button">${itemValue}</a>
            </#if>
            <#assign j=j+1>
        </#list>
    </div>
    <#assign i=i+1>
</#list>
</div>






</body>
</html>