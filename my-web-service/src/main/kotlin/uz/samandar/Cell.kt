package uz.samandar
class Cell(var x:Int,var y:Int) {
    var status:String=CELL_STATUS_EMPTY
    override fun toString(): String {
        return "Cell(status='$status')"
    }

    fun ravno(other: Cell): Boolean {
        return this.x==other.x&&this.y==other.y
    }
}