package uz.samandar
import java.util.concurrent.atomic.AtomicInteger

class Board {
    var cells= arrayListOf<Cell>()
    var ships=arrayListOf<Ship>()
    var checkShipsQuantity=arrayOf(4,3,2,1)

    fun addShips(size:Int,cell: Cell,direction:String):Boolean {
        println("in BOard x->${cell.x},y->${cell.y},$size,$direction")
        var tempShip = Ship(size)
        if (tempShip.make(cell, direction)) {
            if (checkShip(tempShip)) {
                if(checkShipsQuantity[size-1]>0) {
                    checkShipsQuantity[size - 1]--
                    ships.add(tempShip)
                    return true
                }
            }
        }
        return false
    }

    private fun checkShip(currentShip:Ship):Boolean{
        var ax:Int
        var ay:Int
        for (ship in ships) {
            for (cell1 in ship.cells) {
                for (cell in currentShip.cells){
                    for(i in -1..1){
                        ax=cell1.x+i
                        ay=cell1.y+i
                        if((ax==cell.x&&cell1.y==cell.y)||(cell1.x==cell.x&&ay==cell.y))
                        {
                            return false
                        }
                    }
                    if(((cell1.x-1)==cell.x&&(cell1.y+1)==cell.y)||((cell1.x+1)==cell.x&&(cell1.y+1)==cell.y)){
                        return false
                    }
                    if(((cell1.x-1)==cell.x&&(cell1.y-1)==cell.y)||((cell1.x+1)==cell.x&&(cell1.y-1)==cell.y)){
                        return false
                    }

                }
            }
        }
        return true
    }

    fun fire(cell:Cell):String {
        var statusAction: String
        var diedFlag=0
        if (checkBoundaryForCell(cell)) {
            for (cellInAction in cells) {
                if (cellInAction.ravno(cell)) {
                    return CELL_STATUS_USED
                }
            }
            cells.add(cell)
            for (shipInList in ships) {
                statusAction = shipInList.fire(cell)
                if (statusAction == SHIP_STATUS_FIRED) {
                    return statusAction
                }
                if(statusAction==SHIP_STATUS_DIED){
                    println(SHIP_STATUS_FIRED)
                    diedFlag=1
                    return statusAction
                }
            }
            return SHIP_STATUS_MISSED
        }
        return WARNING_OUT_OF_GAME_ZONE
    }


    private fun checkBoundaryForCell(cell:Cell):Boolean{
        if (cell.x < BOARD_BOUNDARY_MIN_X
                || cell.x > BOARD_BOUNDARY_MAX_X
                || cell.y < BOARD_BOUNDARY_MIN_Y
                || cell.y > BOARD_BOUNDARY_MAX_Y
        ) return false
        return true
    }
}