package uz.samandar


import io.vertx.core.AbstractVerticle
import java.math.BigDecimal

class Server : AbstractVerticle() {
    override fun start() {
        vertx.createHttpServer()
                .requestHandler { req ->
                    val a=req.getParam("a")
                    val b=req.getParam("b")
                    val bdA=BigDecimal(a)
                    val bdB=BigDecimal(b)
                    req.response()
                            .putHeader("content-type", "text/plain")
                            .end("Server: $a+$b=${bdA+bdB}")
                }.listen(8080)
    }
}
