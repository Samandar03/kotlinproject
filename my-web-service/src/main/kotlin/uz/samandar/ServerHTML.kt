package uz.samandar

import io.vertx.core.AbstractVerticle
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.StaticHandler
import uz.samandar.controller.BoardController
import java.math.BigDecimal

class ServerHTML: AbstractVerticle()   {


    override fun start() {

        val boardController=BoardController(vertx)
        var router = Router.router(vertx)
        router.route("/welcome").handler(boardController::welcomeHandler)
        router.route("/welcome/:player").handler(boardController::indexHandler)
        router.route("/welcome/:player/:x/:y").handler(boardController::coordinateHandler)
        router.route("/welcome/:player/:size").handler(boardController::sizeHandler)
        router.route("/welcome/:player/:size/:direction/:a").handler(boardController::directionHandler)
        router.route("/welcome/:player/play/:counterOfShips/:k/:l").handler(boardController::playHandler)
        router.route("/welcome/:player/play/:x/:y/:z/:e").handler(boardController::fireHandler)


        router.route("/webroot/*").handler(StaticHandler.create())
        
        vertx.createHttpServer()
                .requestHandler(router)
                .listen(8080)
    }
}