package uz.samandar
fun main(args:Array<String>){
    var board=Board()
    var result:String
    var status:Boolean
    var cell:Cell
    var isWin=0
    var gameOver:Boolean=true
    println("Please enter Ship's coordinates ")
    println("Example : n->size x y (N,E,W,S)->direction")
    var builtShip:Int=1
    while(builtShip!=11)
    {
        print("#$builtShip ")
        var (size,a,b,direction)= readLine()!!.split(" ")
        cell=Cell(a.toInt(),b.toInt())
        when(direction){
            "E"->SHIP_DIRECTION_EAST
            "S"->SHIP_DIRECTION_SOUTH
            "W"->SHIP_DIRECTION_WEST
            else->SHIP_DIRECTION_NORTH
        }
        status=board.addShips(size.toInt(),cell,direction)
        if(status) {
            print(" " + status)
            builtShip++
            println()
        }
        else {
            println()
        }
    }
    while(gameOver){
        var (a,b)= readLine()!!.split(" ")
        cell=Cell(a.toInt(),b.toInt())
        result=board.fire(cell)
        println(result)
        if(result==SHIP_STATUS_DIED)
        {
            isWin++
        }
        if(isWin==10){
            gameOver=false
            println("Game is Over!!! You Loose!!")
        }
    }
}