package uz.samandar

import io.vertx.core.Vertx
import io.vertx.ext.web.client.WebClient


fun main(args: Array<String>) {
    println("Hello, World")
    val vertex = Vertx.vertx()
    vertex.deployVerticle(Server())


    var inAction=true
    var client = WebClient.create(vertex)
    while(inAction){
        val (a,b)= readLine()!!.split(" ")
        client
                .get(8080, "localhost","/?a=$a&b=$b")
                .send ({ ar ->
            if (ar.succeeded()) {
                // Obtain response
                var response = ar.result()

                println("Received from ${response.body()}")
            }
        })

    }
}




