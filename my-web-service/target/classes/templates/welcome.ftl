<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Table</title>
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Samandar">
    <link rel="stylesheet" href="/webroot/css/foundation.css">
    <link rel="stylesheet" href="/webroot/css/style.css">
</head>
<body>

<h6>Welcome to SeaBattle!</h6>
<br>
<h6>Please choose Player Position : </h6>
<br>
<div id="defineUser">

    <#if isPlayer1==0>
        <#assign turn=1>
        <a href="/welcome/${turn}" class="radius medium secondary button">PLAYER 1</a>
    </#if>
    <#if isPlayer2==0>
        <#assign turn=2>
        <a href="/welcome/${turn}" class="radius medium secondary button">PLAYER 2</a>
    </#if>
    <#if isPlayer2==1&&isPlayer1==1>

        <a href="#" class="radius medium alert button">PLAYER 1 is chosen!</a>
        <a href="#" class="radius medium alert button">PLAYER 2 is chosen!</a>
    </#if>


</div>


</body>
</html>